import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { SnackbarService } from './snackbar.service';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import {jwtDecode} from "jwt-decode";
import { GlobalConstants } from '../shared/global-constants';

@Injectable({
  providedIn: 'root'
})
export class RouteGuardService {

  constructor(public auth:AuthService,
    public router:Router,
    public snackbarService:SnackbarService) { }

    canActivate(route:ActivatedRouteSnapshot):boolean{
      let expectedRoleArray=route.data;
      console.log("==========",expectedRoleArray);
      expectedRoleArray=expectedRoleArray.expectedRole;
      console.log("==========",expectedRoleArray);


      const token:any=localStorage.getItem('token');
      var tokenPayload:any;
      try{
        tokenPayload= jwtDecode(token);

      }catch(err){
        localStorage.clear();
        this.router.navigate(['/'])
      }
      let expectedRole='';
      for(let i=0;i<expectedRoleArray.length;i++){
        if(expectedRoleArray[i] ==tokenPayload.role){expectedRole=tokenPayload.role;}
      }
      if (tokenPayload.role == 'user' || tokenPayload.role == 'admin'){
        if (this.auth.isAhuthenticated() && tokenPayload.role == expectedRole){
          return true;
        }
        this.snackbarService.openSnackBar(GlobalConstants.unauthorized,GlobalConstants.error);
        this.router.navigate(['/cafe/dashboard']);
        return false;
      }
      else{
        this.router.navigate(['/']);
        localStorage.clear();
        return false;
      }
    }
}

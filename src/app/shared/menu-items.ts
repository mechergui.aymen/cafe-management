import { Injectable } from "@angular/core";
import { Interface } from "readline";

export interface Menu{
    state:string;
    name:string;
    type:string;
    icon:string;
    role:string;
}

const MENUITEMS = [
    {state: 'dashboard',
     name: 'dashboard',
     type: 'link',
     icon: 'dashboard',
     role: ''} 
]
@Injectable()
export class MenuItems{
    getMenuItems():Menu[]{
        return MENUITEMS;
    }
}